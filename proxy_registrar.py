#!/usr/bin/python3

import socketserver
import sys
import json
import socket
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from time import strftime, strptime, time, localtime


class LogWriter:
    def __init__(self, log_file):
        self.file = log_file

    def write(self, message):
        with open(self.file, "a") as log_file:
            log_file.write(message + "\n")

    def get_date(self):
        now = localtime(time())
        ss = now.tm_hour * 3600 + now.tm_min * 60 + now.tm_sec
        now_str = strftime(("%Y-%m-%d"), now)
        return now_str + " " + str(ss)

    def received(self, address, message):
        line = self.get_date() + " Received from "
        line += address + " " + message.replace("\r\n", " ")
        self.write(line)

    def sent(self, address, message):
        line = self.get_date() + " Sent to "
        line += address + " " + message.replace("\r\n", " ")
        self.write(line)

    def start(self):
        line = self.get_date() + " Starting..."
        self.write(line)

    def finish(self):
        line = self.get_date() + " Finish... "
        self.write(line)


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    ip_dicc = {}

    def register2json(self):
        """
        Creación de fichero json
        """
        fichero_json = list_config["database", "path"]

        with open(fichero_json, "w") as fichero:
            json.dump(self.ip_dicc, fichero, indent=4)

    def json2register(self):
        try:
            fichero_json = list_config["database", "path"]

            with open(fichero_json, "r") as fichero:
                self.ip_dicc = json.load(fichero)
        except:
            self.ip_dicc = {}

    def get_expires_time(self, expires):
        return strftime('%Y-%m-%d %H:%M:%S', localtime(time()+expires))

    def save_client(self, data):
        username = data.split("\r\n")[0].split()[1].split(":")[1]
        user_port = data.split("\r\n")[0].split()[1].split(":")[-1]
        try:
            expires = int(data.split("\r\n")[1].split()[1])
        except:
            return "SIP/2.0 400 Bad Request\r\n"

        if username in self.ip_dicc:
            if expires == 0:
                del self.ip_dicc[username]
            else:
                # Aqui se actualiza el tiempo de expiracion del cliente
                x = self.get_expires_time(expires)
                self.ip_dicc[username]["expires"] = x
            response = "SIP/2.0 200 OK\r\n"
        else:
            if expires == 0:
                response = "SIP/2.0 404 User Not Found\r\n"
            else:
                self.ip_dicc[username] = {
                    "ip": self.client_address[0],
                    "port": int(user_port),
                    "expires": self.get_expires_time(expires)
                }
                response = "SIP/2.0 200 OK\r\n"
        return response

    def expires_client(self):
        dicc_copy = self.ip_dicc.copy()
        now = localtime(time())
        for username in dicc_copy:
            x = dicc_copy[username]["expires"]
            if now >= strptime(x, '%Y-%m-%d %H:%M:%S'):
                del self.ip_dicc[username]

    def resent(self, src, dst, data):
        if src:
            if src not in self.ip_dicc:
                return "SIP/2.0 404 User Not Found\r\n"
        if dst not in self.ip_dicc:
            return "SIP/2.0 404 User Not Found\r\n"

        ip = self.ip_dicc[dst]["ip"]
        port = self.ip_dicc[dst]["port"]
        address = ip + ":" + str(port)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip, port))
            print("Enviando:", data)
            my_socket.send(bytes(data, 'utf-8'))
            log_writer.sent(address, data)
            response = my_socket.recv(1024).decode("utf-8")
        return response

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2register()
        self.expires_client()
        decoded_info = self.rfile.read().decode('utf-8')
        address = self.client_address[0] + ":" + str(self.client_address[1])
        log_writer.received(address, decoded_info)
        print(decoded_info)
        method = decoded_info.split()[0]

        if method == "REGISTER":
            # Aqui se guarda al cliente
            response = self.save_client(decoded_info)

        elif method in ["INVITE", "ACK", "BYE"]:
            if method == "INVITE":
                user_src = decoded_info.split("\r\n")[5]
                user_src = user_src.split()[0].split("=")[1]
            else:
                user_src = ""
            user_dst = decoded_info.split()[1].split(":")[1]

            # Se reenvia al cliente al que se le hace INVITE
            response = self.resent(user_src, user_dst, decoded_info)
            if method == "ACK":
                response = None
        else:
            response = "SIP/2.0 405 Method Not Allowed\r\n"

        if response:
            log_writer.sent(address, response)
            self.wfile.write(bytes(response, "utf-8"))
        self.register2json()


class XMLHandler(ContentHandler):
    current_tag = ""
    parsed_config = {}

    """" Inicializa XMLHandler"""
    def __init__(self):

        self.Labels = {
            "account": ["username", "ip", "puerto"],
            "database": ["path", "passwdpath"],
            "log": [""],
        }

    def startElement(self, tag, attributes):
        self.current_tag = tag
        if tag in self.Labels:
            for attr in self.Labels[tag]:
                if attr != '':
                    if attr != "log":
                        att = '127.0.0.1'
                        y = attributes.get(attr, '')
                        self.parsed_config[tag, attr] = y

    def characters(self, content):
        if self.current_tag == 'log':
            self.parsed_config[self.current_tag, ""] = content

    def endElement(self, name):
        self.current_tag = ""

    def get_tags(self):
        return self.parsed_config


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    if len(sys.argv) != 2:
        sys.exit("Usage: python3 proxy_registrar.py config")

    parser = make_parser()
    handler = XMLHandler()
    parser.setContentHandler(handler)
    parser.parse(open(sys.argv[1]))
    list_config = handler.get_tags()

    serverProxy_Puerto = int(list_config["account", "puerto"])
    serverProxy_Ip = list_config["account", "ip"]
    log_writer = LogWriter(list_config["log", ""])

    prx_data = (serverProxy_Ip, serverProxy_Puerto)
    serv = socketserver.UDPServer(prx_data, SIPRegisterHandler)
    print("Server MyServer listening at port " + str(serverProxy_Puerto))

    try:
        log_writer.start()
        serv.serve_forever()

    except KeyboardInterrupt:
        log_writer.finish()
        print("Finalizado servidor")
