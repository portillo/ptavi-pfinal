#!/usr/bin/python3

import socketserver
import sys
import simplertp
import random
import os
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from time import strftime, time, localtime


class LogWriter:
    def __init__(self, log_file):
        self.file = log_file

    def write(self, message):
        with open(self.file, "a") as log_file:
            log_file.write(message + "\n")

    def get_date(self):
        now = localtime(time())
        ss = now.tm_hour * 3600 + now.tm_min * 60 + now.tm_sec
        now_str = strftime(("%Y-%m-%d"), now)
        return now_str + " " + str(ss)

    def received(self, address, message):
        line = self.get_date() + " Received from "
        line += address + " " + message.replace("\r\n", " ")
        self.write(line)

    def sent(self, address, message):
        line = self.get_date() + " Sent to "
        line += address + " " + message.replace("\r\n", " ")
        self.write(line)

    def start(self):
        line = self.get_date() + " Starting..."
        self.write(line)

    def finish(self):
        line = self.get_date() + " Finish... "
        self.write(line)


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    rtp_info = []

    """
    Echo server class
    """
    def create_csrc(self):
        n = random.randint(5, 15)
        csrc = []
        for i in range(n):
            csrc.append(random.randint(1, 75))
        return csrc

    def receive_ack(self):
        csrc = self.create_csrc()
        RTP_header = simplertp.RtpHeader()
        bit = random.choice([0, 1])
        info_rtp = self.rtp_info[0]
        info_rtp1 = self.rtp_info[1]
        RTP_header.set_header(marker=bit, cc=random.randint(0, 15))
        os.system("cvlc rtp://@" + info_rtp + ":" + str(info_rtp1) + " &")
        RTP_header.setCSRC(csrc)
        audio = simplertp.RtpPayloadMp3(list_config["audio", "path"])
        simplertp.send_rtp_packet(RTP_header, audio, info_rtp, info_rtp1)
        return None

    def receive_bye(self):
        if len(self.rtp_info) != 0:
            self.rtp_info.pop()
            self.rtp_info.pop()
        return "SIP/2.0 200 OK\r\n"

    def receive_invite(self, data):
        if len(self.rtp_info) == 0:
            self.rtp_info.append(data.split("\r\n")[5].split()[1])
            self.rtp_info.append(int(data.split("\r\n")[-3].split()[1]))
        composed_message = """SIP/2.0 100 Trying\r\n\r
SIP/2.0 180 Ringing\r\n\r
SIP/2.0 200 OK\r
Content-type: application/sdp\r
Content-length: #LENGTH#"""
        sdp_body = """\r\n\r\nv=0\r
o={username} {client_ip}\r
s=session\r
t=0\r
m=audio {rtp_Port} RTP\r\n""".format(
            username=list_config["account", "username"],
            puerto=list_config["uaserver", "puerto"],
            client_ip=list_config["uaserver", "ip"],
            rtp_Port=list_config["rtpaudio", "puerto"]
        )
        x = str(len(sdp_body))
        composed_message = composed_message.replace("#LENGTH#", x)
        return composed_message + sdp_body

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        decoded_info = self.rfile.read().decode('utf-8')
        address = self.client_address[0] + ":" + str(self.client_address[1])
        log_writer.received(address, decoded_info)

        print(self.rtp_info)

        request_method = decoded_info.split()[0]
        if request_method == "INVITE":
            # Aqui se guarda al cliente
            response = self.receive_invite(decoded_info)
        elif request_method == "ACK":
            # Aqui se guarda al cliente
            response = self.receive_ack()
        elif request_method == "BYE":
            # Aqui se guarda al cliente
            response = self.receive_bye()
        else:
            response = "SIP/2.0 405 Method Not Allowed\r\n"

        if response:
            log_writer.sent(address, response)
            self.wfile.write(bytes(response, "utf-8"))


class XMLHandler(ContentHandler):
    current_tag = ""
    parsed_config = {}

    """" Inicializa XMLHandler"""
    def __init__(self):

        self.Labels = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
        }

    def startElement(self, tag, attributes):
        self.current_tag = tag
        if tag in self.Labels:
            for attr in self.Labels[tag]:
                if attr != '':
                    self.parsed_config[tag, attr] = attributes.get(attr, '')

    def get_tags(self):
        return self.parsed_config


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    if len(sys.argv) != 2:
        sys.exit("Usage: python3 uaserver.py config")

    parser = make_parser()
    handler = XMLHandler()
    parser.setContentHandler(handler)
    parser.parse(open(sys.argv[1]))
    list_config = handler.get_tags()

    user = list_config["account", "username"]
    server_Puerto = int(list_config["uaserver", "puerto"])
    server_Ip = list_config["uaserver", "ip"]
    rtp_Port = list_config["rtpaudio", "puerto"]
    proxy_Port = int(list_config["regproxy", "puerto"])
    proxy_Ip = list_config["regproxy", "ip"]
    log_writer = LogWriter(list_config["log", "path"])

    praddress = (server_Ip, server_Puerto)
    serv = socketserver.UDPServer(praddress, SIPRegisterHandler)
    print("Listening...")

    try:
        log_writer.start()
        serv.serve_forever()

    except KeyboardInterrupt:
        log_writer.finish()
        print("Finalizado servidor")
