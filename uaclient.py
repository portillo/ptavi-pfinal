#!/usr/bin/python3

import sys
import socket
import random
import simplertp
import time
import os
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class LogWriter:
    def __init__(self, log_file):
        self.file = log_file

    def write(self, message):
        with open(self.file, "a") as log_file:
            log_file.write(message + "\n")

    def get_date(self):
        now = time.localtime(time.time())
        ss = now.tm_hour * 3600 + now.tm_min * 60 + now.tm_sec
        now_str = time.strftime(("%Y-%m-%d"), now)
        return now_str + " " + str(ss)

    def received(self, address, message):
        line = self.get_date() + " Received from "
        line += address + " " + message.replace("\r\n", " ")
        self.write(line)

    def sent(self, address, message):
        line = self.get_date() + " Sent to "
        line += address + " " + message.replace("\r\n", " ")
        self.write(line)

    def start(self):
        line = self.get_date() + " Starting..."
        self.write(line)

    def finish(self):
        line = self.get_date() + " Finish... "
        self.write(line)


class SIPMessages:
    def __init__(self, tags):
        self.tags = tags

    def get_message(self, method, option):
        if method == "REGISTER":
            return self.get_register(option)
        if method == "INVITE":
            return self.get_invite(option)
        if method == "ACK":
            return self.get_ack(option)
        if method == "BYE":
            return self.get_bye(option)

    def get_register(self, option):
        composed_message = """REGISTER sip:{username}:{puerto} SIP/2.0\r
Expires: {option}""".format(
            username=self.tags["account", "username"],
            puerto=self.tags["uaserver", "puerto"],
            option=option
        )
        return composed_message

    def get_invite(self, option):
        composed_message = """INVITE sip:{option} SIP/2.0\r
Content-type: application/sdp\r
Content-length: #LENGTH#\r
\r\nv=0\r
o={username} {client_ip}\r
s=session\r
t=0\r
m=audio {rtp_Port} RTP\r\n""".format(
            username=self.tags["account", "username"],
            puerto=self.tags["uaserver", "puerto"],
            option=option,
            client_ip=self.tags["uaserver", "ip"],
            rtp_Port=self.tags["rtpaudio", "puerto"]
        )
        leng_message = str(len(composed_message))
        composed_message = composed_message.replace("#LENGTH#", leng_message)
        return composed_message

    def get_ack(self, option):
        composed_message = """ACK sip:{option} SIP/2.0\r\n""".format(
            option=option
        )
        return composed_message

    def get_bye(self, option):
        composed_message = """BYE sip:{option} SIP/2.0\r\n""".format(
            option=option
        )
        return composed_message


class XMLHandler(ContentHandler):
    current_tag = ""
    parsed_config = {}

    """" Inicializa XMLHandler"""
    def __init__(self):

        self.Labels = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
        }

    def startElement(self, tag, attributes):
        self.current_tag = tag
        if tag in self.Labels:
            for attr in self.Labels[tag]:
                if attr != '':
                    self.parsed_config[tag, attr] = attributes.get(attr, '')

    def get_tags(self):
        return self.parsed_config


def accept_invite(data):
    ok = True
    if "100 Trying" not in data:
        ok = False
    if "180 Ringing" not in data:
        ok = False
    if "200 OK" not in data:
        ok = False
    return ok


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    if len(sys.argv) != 4:
        sys.exit("Usage: python3 uaclient.py config method option")

    CONFIG = sys.argv[1]
    METHOD = sys.argv[2]
    OPTION = sys.argv[3]

    parser = make_parser()
    handler = XMLHandler()
    parser.setContentHandler(handler)
    parser.parse(open(sys.argv[1]))
    list_config = handler.get_tags()
    sip_message = SIPMessages(list_config)

    user = list_config["account", "username"]
    server_Puerto = int(list_config["uaserver", "puerto"])
    server_Ip = list_config["uaserver", "ip"]
    rtp_Port = list_config["rtpaudio", "puerto"]
    proxy_Port = int(list_config["regproxy", "puerto"])
    proxy_Ip = list_config["regproxy", "ip"]
    composed_message = sip_message.get_message(METHOD, OPTION)
    log_writer = LogWriter(list_config["log", "path"])

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((proxy_Ip, proxy_Port))
    print("Enviando:", composed_message)

    address = proxy_Ip + ":" + str(proxy_Port)
    log_writer.sent(address, composed_message)
    my_socket.send(bytes(composed_message, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024).decode('utf-8')
    log_writer.received(address, data)
    print('Recibido -- ', data)

    if METHOD == "INVITE":
        if accept_invite(data):
            composed_message = sip_message.get_message("ACK", OPTION)
            log_writer.sent(address, composed_message)
            my_socket.send(bytes(composed_message, 'utf-8') + b'\r\n')
            ip = data.split("\r\n")[-5].split()[-1]
            port = int(data.split("\r\n")[-2].split()[1])

            n = random.randint(5, 15)
            csrc = []
            for i in range(n):
                csrc.append(random.randint(1, 75))

            RTP_header = simplertp.RtpHeader()
            bit = random.choice([0, 1])
            RTP_header.set_header(marker=bit, cc=random.randint(0, 15))

            os.system("cvlc rtp://@" + ip + ":" + str(port) + " &")

            RTP_header.setCSRC(csrc)
            audio = simplertp.RtpPayloadMp3(list_config["audio", "path"])
            simplertp.send_rtp_packet(RTP_header, audio, ip, port)

print("Socket terminado.")
